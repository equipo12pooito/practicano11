package clases.ito.poo;

import java.util.ArrayList;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import app.ito.poo.Aplicacion;

public class Persistencia {

	public static void grabaLista(ArrayList<Transporte> lista) {
		ObjectOutputStream file;
		try {
			file = new ObjectOutputStream(new FileOutputStream("datos.dat"));
			for (Transporte a: lista)
				file.writeObject(a);
			file.close();
		}catch(Exception e) {
			System.err.println(e.getMessage());
		}
	}
	
	public static ArrayList<Transporte> leeLista(){
		ArrayList<Transporte> l=new ArrayList<Transporte>();
		ObjectInputStream file=null;
		Transporte t=null;
		try {
			file=new ObjectInputStream(new FileInputStream("datos.dat"));
			while((t=(Transporte)file.readObject())!=null)
				l.add(t);
		}catch (Exception e) {
			System.err.println(e.getCause());
		}
		return l;
	}
	
}